import React from 'react'
import AppHeader from '../../components/AppHeader/AppHeader'
import ProductList from '../../components/ProductList/ProductList'
import CartContext from './CartContext'

class ProductListPage extends React.Component {

  addProduct = async (product) => {
    await this.setState(
      (prevState) => {
        const { products } = prevState
        products.push(product)
        return { products: products }
      }, ()=>localStorage.setItem('cartProducts', JSON.stringify(this.state.products))
    )
  }

  removeProduct = (id) => {
    this.setState(
      (prevState) => {
        const { products } = prevState
        const index = products.findIndex(product => product.id === id)
        products.splice(index, 1)
        return { products: products }
      }, ()=>localStorage.setItem('cartProducts', JSON.stringify(this.state.products))
    )
  }

  state = {
    products: JSON.parse(localStorage.getItem('cartProducts')) || [],
    addProduct: this.addProduct,
    removeProduct: this.removeProduct
  }

  render() {
    return (
      <React.Fragment>
        <CartContext.Provider value={ this.state }>
          <AppHeader/>
          <main className="container">
            <ProductList/>
          </main>
        </CartContext.Provider>
      </React.Fragment>
    )
  }
}

export default ProductListPage
