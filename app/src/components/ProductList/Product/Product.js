import React from 'react'
import classes from './Product.module.css'
import onErrorImg from './../../../assets/images/default.jpg'

class Product extends React.PureComponent {

  state = {
    selectedImageUrl: '',
    selectedIndex: 0
  }

  buttonOnClick = () => {
    this.props.handleClick(this.props.product)
  }

  imageOnClick = (event) => {
    this.setState({
      selectedImageUrl: event.target.attributes.src.value,
      selectedIndex: parseInt(event.target.dataset.index)
    })
  }

  imageOnError = (event) => {
    event.target.src = onErrorImg
  }

  uiRenderPhotos = () => {
    const { selectedIndex } = this.state
    const { images, name } = this.props.product
    return images.map((image, index) => {
      return <img src={ image } alt={ name }
                  key={ index }
                  data-index={ index }
                  className={selectedIndex === index ? classes.selected : null}
                  onError={this.imageOnError}
                  onClick={this.imageOnClick} />
    })
  }

  render() {
    const { selectedImageUrl } = this.state
    const { images, price, name } = this.props.product
    return (
      <article className={ classes.product }>
        <div className={ classes.imgGallery }>
          <div className={ classes.list}>
            { this.uiRenderPhotos() }
          </div>
          <div className={ classes.selected }>
            <img src={selectedImageUrl || images[0]} alt={ name } onError={this.imageOnError}/>
          </div>
        </div>
        <div className={ classes.informations }>
          <h1 className={ classes.name }>{ name }</h1>
          <div className={ classes.paymentAction }>
            <div className={ classes.payment }>
              <p className={ `${classes.installment} txtPositiveColor` }>{ price.installments }x { price.installmentValue.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' }) }</p>
              <p className={ classes.price }>ou <span className="txtPositiveColor">{ price.value.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' }) }</span> à vista</p>
            </div>
            <div className={ classes.action }>
              <button className="button positive"
                      onClick={this.buttonOnClick}>Adicionar ao carrinho <i className="iconRightArrow"></i></button>
            </div>
          </div>
        </div>
      </article>
    )
  }
}

export default Product