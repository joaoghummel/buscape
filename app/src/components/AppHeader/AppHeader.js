import React from 'react'

import logo from './../../assets/brand/logo.png'
import menu from './../../assets/icons/menu.png'
import classes from './AppHeader.module.css'
import CartContext from '../../pages/ProductListPage/CartContext'
import ProductPreview from './ProductPreview/ProductPreview'

class AppHeader extends React.PureComponent {

  state = {
    isOpen: false
  }

  static contextType = CartContext

  menuOnClick = () => {
    this.setState((prevState) => {
      return { isOpen: !prevState.isOpen }
    })
  }

  buttonOnClick = (id) => {
    const { products, removeProduct } = this.context
    products.length === 1 && this.setState({isOpen: false})
    removeProduct(id)
  }

  uiRenderPreview = () => {
    const { products } = this.context
    const { isOpen } = this.state
    
    return isOpen && products.length ? (
      <div className={ classes.cartPreview }>
        { this.uiRenderProducts() }
        <div className={ classes.subtotal }>
          <p className={classes.heading}>subtotal</p>
          <p>
            {products[0].price.installments}x 
            {
              products.map(product => {
                return product.price.installmentValue
              }).reduce((prev, next)=>{
                return prev + next
              },0).toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })
            }
          </p>
          <p>
            ou {
              products.map(product => {
                return product.price.value
              }).reduce((prev, next)=>{
                return prev + next
              },0).toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })
            } à vista
          </p>
        </div>
      </div>

    ) : null
  }

  uiRenderProducts = () => {
    const { products } = this.context
    return products.length ? (
      <ul className={ classes.productList}>
        {
          products.map((product, index) => {
            return (
              <ProductPreview key={`${index} ${product.id}`}
                              product={ product }
                              handleClick={ this.buttonOnClick } />
            )
          })
        }
      </ul>
    ) : null
  }

  render() {
    const { products } = this.context
    return (
      <header className={ classes.appHeader }>
        <a className={ classes.logo } href="/">
          <img src={ logo } alt="Buscape"/>
        </a>
        <div className={ classes.menu } onClick={this.menuOnClick}>
          <img src={ menu } alt="Ícone do menu"/>
          { products.length
            ? <div className={ classes.counter }>{ products.length }</div>
            : null
          }
        </div> 
        { this.uiRenderPreview() }
      </header>
    )
  }
}

export default AppHeader
