import React from 'react'

import classes from './ProductPreview.module.css'
import onErrorImg from './../../../assets/images/default.jpg'

class ProductPreview extends React.PureComponent {

  buttonOnClick = () => {
    const { product, handleClick } =  this.props
    handleClick(product.id)
  }

  imageOnError = (event) => {
    event.target.src = onErrorImg
  }

  render() {
    const { images, name, price } = this.props.product
    return (
      <li className={ classes.product }>
        <div className={ classes.image }>
          <img src={ images[0] } alt={ name } onError={ this.imageOnError }/>
        </div>
        <div className={ classes.informations }>
          <p className={ classes.name }>{ name }</p>
          <p className={ classes.installment }>{ price.installments }x { price.installmentValue.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' }) }</p>
          <p className={ classes.price }>ou { price.value.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' }) } à vista</p>
        </div>
        <div className={ classes.remove } onClick={this.buttonOnClick}>X</div>
      </li>
    )
  }

}

export default ProductPreview