## Desenvolvimento
O projeto foi realizado utilizado o **create-react-app**

Para resolver o conceito do carrinho, utilizei a ContextAPI

Para layout utilizer o CSS Modules com flexbox (Sem framework)


## Rodar projeto
Para rodar o projeto:
* Clonar o repositório
* Instalar dependências: **yarn install**
* Rodar servidor local: **yarn start**